<?php

/**
 * Applique notre hash maison à une chaîne donnée.
 * 
 * @param string $password Chaîne à laquelle appliquer le hash.
 * 
 * @return string Résultat du hash.
 */
function hashPassword( string $password ): string
{
    // On passe le hash avec un algorithme Sha256 sur les 3 éléments à encoder
    $salt_hash = hash( 'sha256', SALT );
    $pepper_hash = hash( 'sha256', PEPPER );
    $password_hash = hash( 'sha256', $password );

    // On concatène les 3 hash obtenus
    $hash_string = $salt_hash . $password_hash . $pepper_hash;

    // On passe à nouveau le hash sur le résultat précédent
    $result = hash( 'sha256', $hash_string );

    return $result;
}

// Vérifie si on est connecté
// Redirige vers le login si ce n'est pas le cas
function checkLogedIn(): void
{
    if( !empty( $_SESSION[ 'user' ] ) ) {
        return;
    }

    header( 'Location: http://local.tp-forum-php.net/?url=login' );
    die();
}

function displayUsername(): void
{
    if( !empty( $_COOKIE[ 'username' ] ) ){
        echo '<h1>Bienvenue sur le Faux-Rôme, ' . $_COOKIE[ 'username' ] . '.</h1>';
    }else{
        echo '<h1>Bienvenue sur le Faux-Rôme.</h1>';
    }
}