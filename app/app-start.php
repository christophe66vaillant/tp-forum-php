<?php

// Constantes de chemins
define( 'PATH_CONTROLLERS', 'app/controllers/' );
define( 'PATH_VIEWS', 'app/views/' );
define( 'PATH_MODELS', 'app/models/' );

//  Démarrage de la session
session_start();

$mysql = mysqli_connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );

if( $mysql->connect_error ){
    die( 'Connexion impossible : ' . $mysql->connect_error );
}

$page = !empty( $_GET[ 'url' ] ) ? $_GET[ 'url' ] : 'accueil';

switch( $page ) {

    case 'login':
        require_once PATH_MODELS . 'login-model.php';
        break;

    case 'register':
        require_once PATH_MODELS . 'register-model.php';
        break;

    case 'accueil':
        require_once  PATH_MODELS . 'category-model.php';
        break;

    case 'subject':
        require_once  PATH_MODELS . 'subject-model.php';
        break;

    default:
        break;
}
