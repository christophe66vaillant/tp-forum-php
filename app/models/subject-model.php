<?php

$category = $_GET[ 'category' ];

var_dump( $category );

checkLogedIn();

if( isset( $_GET['disconnect'] ) ) {
    unset( $_SESSION[ 'user' ] );
    header( 'Location: http://local.tp-forum-php.net/?url=login' );
    die();
}

// Si on a posté le formulaire
if( $_SERVER[ 'REQUEST_METHOD' ] === 'POST' && !empty( $_POST[ 'add-subject' ] ) ) {
    newSubject();
}

function newSubject(): ?array
{
    global $mysql;

    $result = [];
    $req = 'INSERT INTO subjects (`title`, category_id) VALUES (?, ?)';

    if( $stmt = mysqli_prepare( $mysql, $req ) ){
        $name = $_POST[ 'add-subject' ];

        mysqli_stmt_bind_param( $stmt, 'si', $name, $category );
        mysqli_stmt_execute( $stmt );

        $resumt = mysqli_stmt_get_result( $stmt );
    }
    return $result;
}

function displaySubject(): void
{
    global $mysql;
    $result = mysqli_query( $mysql, 'SELECT * FROM subjects ORDER BY id' );

    echo '<ul>';

    while( $row = mysqli_fetch_assoc( $result ) ){
        echo '<li><a href="/?url=subject=' . $row['id'] . '">' . $row[ 'title' ] . '</a></li>';
    }
    echo '</ul>';
}

require_once PATH_VIEWS . 'subject.php';
