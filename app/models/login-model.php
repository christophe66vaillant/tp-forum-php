<?php

// Si on a posté le formulaire
if( $_SERVER[ 'REQUEST_METHOD' ] === 'POST' ) {

    // On récupères les champs saisis
    $username = !empty( $_POST[ 'username' ] ) ? $_POST[ 'username' ] : '';
    $password = !empty( $_POST[ 'password' ] ) ? $_POST[ 'password' ] : '';

    setcookie( 'username', $username, time() + (3600 * 24 * 2) );

    $q_name = mysqli_query( $mysql, "SELECT `username` FROM users WHERE `username` ='" . $username . "'");

    if( mysqli_num_rows( $q_name ) === 0 ){

        echo 'Ce pseudo n\'existe pas.';

    }else{
        // Tous les champs sont saisis ? => check en BDD
        if( !empty( $username ) && !empty( $password ) ) {
        
        // On appelle le modèle pour trouver l'utilisateur
        $user = getLogin( $username, $password );
        
        // Le modèle renvoie-t-il un utilisateur ? => Enregistrement dans la session
        if( $user !== null ) {
            $_SESSION[ 'user' ] = $user;

            // Redirection vers l'accueil
            // header(): Ajoute à la réponse un en-tête HTTP
            // L'en-tête "Location" ordonne au navigateur d'aller sur la page donnée
            header( 'Location: http://local.tp-forum-php.net/?url=accueil' );

            // die(): Interrompt tout le code PHP
            // Au cas ou la redirection prendrait du temps
            die();
        }
        }
    }
}


function getLogin( string $username, string $password ): ?array
{
    global $mysql;
    
    $result = [];

    $req = 'SELECT *
        FROM users
        WHERE
            username = ?
            AND `password` = ?';

    if( $stmt = mysqli_prepare( $mysql, $req ) ) {

        $password_hash = hashPassword( $password );

        mysqli_stmt_bind_param( $stmt, 'ss', $username, $password_hash );
        mysqli_stmt_execute( $stmt );

        $req_result = mysqli_stmt_get_result( $stmt );

        $result = mysqli_fetch_assoc( $req_result );

        mysqli_stmt_close( $stmt );
    }
    return $result;
}

require_once PATH_VIEWS . 'login.php';
