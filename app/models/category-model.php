<?php

checkLogedIn();

if( isset( $_GET['disconnect'] ) ) {
    unset( $_SESSION[ 'user' ] );
    header( 'Location: http://local.tp-forum-php.net/?url=login' );
    die();
}

// Si on a posté le formulaire
if( $_SERVER[ 'REQUEST_METHOD' ] === 'POST' && !empty( $_POST[ 'add-categorie' ] ) ) {
    newCategorie();
}

function newCategorie(): ?array
{
    global $mysql;

    $result = [];
    $req = 'INSERT INTO categories (`name`) VALUES (?)';

    if( $stmt = mysqli_prepare( $mysql, $req ) ){
        $name = $_POST[ 'add-categorie' ];

        mysqli_stmt_bind_param( $stmt, 's', $name );
        mysqli_stmt_execute( $stmt );

        $resumt = mysqli_stmt_get_result( $stmt );
    }
    return $result;
}

function displayCategorie(): void
{
    global $mysql;
    $result = mysqli_query( $mysql, 'SELECT * FROM categories ORDER BY id' );

    echo '<ul>';

    while( $row = mysqli_fetch_assoc( $result ) ){
        echo '<li><a href="/?url=category=' . $row['id'] . '">' . $row[ 'name' ] . '</a></li>';
    }
    echo '</ul>';
}

require_once PATH_VIEWS . 'category.php';
